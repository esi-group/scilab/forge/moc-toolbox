// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->
mode(-1)
N=256;
M=64;

x=rand(1,N);

sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 1 failed');
end


x=rand(N,1);

sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 2 failed');
end

x=rand(N,N);

sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 3 failed');
end


x=rand(M,N);

sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 4 failed');
end

x=rand(N,M);

sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 5 failed');
end

N=111;M=56;

x=rand(1,N)+%i*rand(1,N);
sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 6 failed');
end

x=rand(N,1)+%i*rand(N,1);
sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 7 failed');
end

x=rand(N,N)+%i*rand(N,N);
sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 8 failed');
end

x=rand(M,N)+%i*rand(M,N);
sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 9 failed');
end

x=rand(N,M)+%i*rand(N,M);
sp1=mtlb_fft(x);
sp2=moc_fft(x);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 10 failed');
end

N=111;M=56; n=5;

x=rand(1,N)+%i*rand(1,N);
sp1=mtlb_fft(x,n);
sp2=moc_fft(x,n);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 11 failed');
end

x=rand(N,1)+%i*rand(N,1);
sp1=mtlb_fft(x,n);
sp2=moc_fft(x,n);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 12 failed');
end

x=rand(N,N)+%i*rand(N,N);
sp1=mtlb_fft(x,n);
sp2=moc_fft(x,n);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 13 failed');
end

x=rand(M,N)+%i*rand(M,N);
sp1=mtlb_fft(x,n);
sp2=moc_fft(x,n);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 14 failed');
end

x=rand(N,M)+%i*rand(N,M);
sp1=mtlb_fft(x,n);
sp2=moc_fft(x,n);
if sum(abs(sp1-sp2))>sqrt(%eps),
  error('moc_fft test 15 failed');
end

N=1024;

x=rand(N/2,N);
tic()
mtlb_fft(x);
t1=toc();
tic()
moc_fft(x);
t2=toc()

if t1<10*t2,
  error('moc_fft test 16 failed');
end
