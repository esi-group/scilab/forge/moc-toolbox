// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->
//
//test
 x1 = [1, 2; 3, 4];
x2 = [2, 4; 1, 3];
 x3 = [4, 3; 2, 1];
 x4 = [3, 1; 4, 2];
//
 assert_checktrue(and((moc_rot90 (x1)== x2 & moc_rot90 (x1, 2) == x3 & moc_rot90 (x1, 3) == x4 & moc_rot90 (x1, 4) == x1 & moc_rot90 (x1, 5) == x2 & moc_rot90 (x1, -1) == x4)));
