// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->

 x = [1, 2, 3; 4, 5, 6; 7, 8, 9];
//
assert_checkequal (moc_circshift (x, 1), [7, 8, 9; 1, 2, 3; 4, 5, 6])
assert_checkequal (moc_circshift (x, -2), [7, 8, 9; 1, 2, 3; 4, 5, 6])
assert_checkequal (moc_circshift (x, [0, 1]), [3, 1, 2; 6, 4, 5; 9, 7, 8]);
assert_checkequal (moc_circshift ([],1), [])
//
assert_checkequal (full (moc_circshift (eye (3,3), 1)), moc_circshift (full (eye (3,3)), 1))
assert_checkequal (full (moc_circshift (eye (3,3), 1)), [0,0,1;1,0,0;0,1,0])
