// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->



//!test
[result, a_idx] = moc_ismember ([1, 2], []);
assert_checkequal (result,  ([0, 0])~=0)
assert_checkequal (a_idx, [0, 0]);

//!test
// [result, a_idx] = moc_ismember ([], [1, 2]);
// assert_checkequal (result,  ([]))
// assert_checkequal (a_idx, []);

//!test
[result, a_idx] = moc_ismember (['a', 'b'], '');
assert_checkequal (result,  ([0, 0])~=0)
assert_checkequal (a_idx, [0, 0]);

//!test
// [result, a_idx] = moc_ismember ({'a', 'b'}, {});
// assert_checkequal (result,  ([0, 0])~=0)
// assert_checkequal (a_idx, [0, 0]);

//!test
// [result, a_idx] = moc_ismember ('', {'a', 'b'});
// assert_checkequal (result, %f)
// assert_checkequal (a_idx, 0);

//!test
// [result, a_idx] = moc_ismember ({}, {'a', 'b'});
// assert_checkequal (result,  ([]))
// assert_checkequal (a_idx, []);

//!test
[result, a_idx] = moc_ismember([1 2 3 4 5], [3]);
assert_checktrue (and (result ==  ([0 0 1 0 0])~=0) & and (a_idx == [0 0 1 0 0]));

//!test
[result, a_idx] = moc_ismember([1 6], [1 2 3 4 5 1 6 1]);
assert_checktrue (and (result ==  ([1 1])~=0) & and (a_idx == [8 7]));

//!test
[result, a_idx] = moc_ismember ([3,10,1], [0,1,2,3,4,5,6,7,8,9]);
assert_checktrue (and (result ==  ([1, 0, 1])~=0) & and (a_idx == [4, 0, 2]));

//!test
[result, a_idx] = moc_ismember ("1.1", "0123456789.1");
assert_checktrue (and (result ==  ([1, 1, 1])~=0) & and (a_idx == [12, 11, 12]));

//!test
[result, a_idx] = moc_ismember([1:3; 5:7; 4:6], [0:2; 1:3; 2:4; 3:5; 4:6], 'rows');
assert_checktrue (and (result ==  ([1; 0; 1])~=0) & and (a_idx == [2; 0; 5]));

//!test
[result, a_idx] = moc_ismember([1.1,1.2,1.3; 2.1,2.2,2.3; 10,11,12], [1.1,1.2,1.3; 10,11,12; 2.12,2.22,2.32], 'rows');
assert_checktrue (and (result ==  ([1; 0; 1])~=0) & and (a_idx == [1; 0; 2]));
