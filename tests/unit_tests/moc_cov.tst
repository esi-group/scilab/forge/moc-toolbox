// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->
//test
x = rand (10,10);
cx1 = moc_cov (x);
cx2 = moc_cov (x, x);
assert_checktrue (size (cx1) == [10, 10] & size (cx2) == [10, 10]);
assert_checkalmostequal (cx1, cx2, 1e1*%eps);

//test
x = [1:3]';
y = [3:-1:1]';
assert_checkalmostequal (moc_cov (x, y), -1, 5*%eps);
assert_checkalmostequal (moc_cov (x, moc_flipud (y)), 1, 5*%eps);
assert_checkalmostequal (moc_cov ([x, y]), [1 -1; -1 1], 5*%eps);

//test
// x = single ([1:3]');
// y = single ([3:-1:1]');
// assert (cov (x, y), single (-1), 5*eps);
// assert (cov (x, flipud (y)), single (1), 5*eps);
// assert (cov ([x, y]), single ([1 -1; -1 1]), 5*eps);

//test
x = [1:5];
c = moc_cov (x);
assert_checktrue (isscalar (c));
assert_checkequal (c, 2.5);

assert_checkequal(moc_cov (5), 0)
//assert (moc_cov (single (5)), single (0))

//test
x = [1:5];
c = moc_cov (x, 0);
assert_checkequal (c, 2.5);
c = moc_cov (x, 1);
assert_checkequal (c, 2);
