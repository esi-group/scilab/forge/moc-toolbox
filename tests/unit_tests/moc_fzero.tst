// -------------------------------------------------------------------------
// moc - Matlab/Octave Compatibility toolbox
// Copyright (C) 2010-2014  Holger Nahrstaedt
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//-------------------------------------------------------------------------
//
//  <-- NO CHECK ERROR OUTPUT -->
//// usage and error testing
////	the Brent's method
//test
 options.abstol=0;
 assert_checkalmostequal (moc_fzero('sin',[-1,2],options), 0)
//test
options.abstol=%eps;
 options.reltol=1e-3;
 assert_checkalmostequal (moc_fzero('tan',[-0.5,1.41],options), 0, %eps,0.01)
//test
options.abstol=1e-3;
assert_checkalmostequal (moc_fzero('atan',[-(10^300),10^290],options), 0, %eps, 1e-3)
//test
 //testfun=inline('(x-1)^3','x');
 deff('[x]=testfun1(x)','x=(x-1)^3');
 options.abstol=0;
 options.reltol=%eps;
 assert_checkalmostequal (moc_fzero("testfun1",[0,3],options), 1, %eps,1e-7)
//test
 //testfun=inline('(x-1)^3+y+z','x','y','z');
 deff('[out]=testfun2(x)','y=22;z=5;out=(x-1)^3+y+z');
 options.abstol=0;
 options.reltol=%eps;
 assert_checkalmostequal (moc_fzero("testfun2",[-3,0],options), -2, %eps,1e-7)
//test
 //testfun=inline('x.^2-100','x');
 deff('[out]=testfun3(x)','out=x.^2-100');
 options.abstol=1e-4;
 assert_checkalmostequal (moc_fzero("testfun3",[-9,300],options),10, %eps,1e-4)
///	`fsolve'
//test
 options.abstol=0.01;
 assert_checkalmostequal (moc_fzero('tan',-0.5,options), 0, %eps, 0.01)
//test
 options.abstol=0;
 assert_checkalmostequal (moc_fzero('sin',[0.5,1],options), 0)
//
