// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.

// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the swt help
mprintf("Updating moc_toolbox\n");
helpdir = fullfile(cwd,"./");
funmat = [
  "moc_cholinv"
  "moc_angle"
  "moc_columns"
  "moc_circshift"
  "moc_confidence"
  "moc_fft"
  "moc_fliplr"
  "moc_flipud"
  "moc_gaussian"
  "moc_ifft"
  "moc_ismember"
  "moc_islogical"
  "moc_null"
  "moc_poly"
  "moc_fzero"
  "moc_polyconf"
  "moc_polyfit"
  "moc_polyval"
  "moc_postpad"
  "moc_prepad"
  "moc_range"
  "moc_rot90"
  "moc_spdiags"
  "moc_squeeze"
  "moc_unique"
  "moc_corr"
  "moc_deal"
  "moc_inpolygon"
  "moc_randi"
  "moc_cov"
  "moc_conv"
  "moc_conv2"
  "moc_corrcov"
  "moc_xcorr"
  "moc_xcorr2"
  "moc_xcov"
  "moc_sumsq"
  "moc_rows"
  "moc_size_equal"
  "moc_unwrap"
  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "swt";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t )
//
